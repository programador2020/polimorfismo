package entidades;

public class TestPagos {
    public static void main(String[] args) {
        System.out.println("Hola");
        //Polimorfismo
        
        
        //Pago con Credito Visa
        PagoCredito miPagoCredito = new PagoCredito();
        miPagoCredito.nombre = "Visa";
        miPagoCredito.comision = 14;
        miPagoCredito.formaPago();
        System.out.println("Comision de " + miPagoCredito.nombre + " es " + miPagoCredito.comision);
        
        PagoCredito miPagoCreditoMaster = new PagoCredito();
        miPagoCreditoMaster.nombre = "MasterCard";
        miPagoCreditoMaster.comision = 10;
        System.out.println("Comision de " + miPagoCreditoMaster.nombre + " es " + miPagoCreditoMaster.comision);
        
        
        //Pago con Paypal
        PagoPaypal miPagoPaylPal = new PagoPaypal();
        miPagoPaylPal.formaPago();
        
        //Pago con Tarjeta de debito
        PagoDebito miPagoDebito = new PagoDebito();
        miPagoDebito.formaPago();
        
        
        //Pago general
        Pago miPago= new Pago();
        miPago.formaPago();
        
        System.out.println("Chau");
    }
}
